#!/usr/bin/env node

const pkg = require('./package.json');
const fs = require('fs');
const path = require('path');
const program = require('commander');

const npmrcFileName = '.npmrc';

program
  .version(pkg.version)
  .option('-r, --registry <registry>', `Specify the registry.`)
  .option('-o, --org <org>', 'Specify the org. By default, no org is used.')
  .option('-a, --always-auth <alias>', 'Boolean always auth')
  .option('-d, --dry-run', 'Do not modify or write any files and print to the console.')
  .parse(process.argv);


const pth = program.registry.replace("https://", "");

const npmrc = `
@${program.org}:registry=${program.registry}
//${pth}/:_password=\${NPM_PASSWORD}
//${pth}/:username=\${NPM_USERNAME}
//${pth}/:email=\${NPM_EMAIL}
//${pth}/:always-auth=${program.alwaysAuth}
`;
const npmrcFile = path.join(process.cwd(), npmrcFileName);
if (!program.dryRun) {
  fs.writeFileSync(npmrcFile, npmrc, 'utf-8');
}
console.log(`${npmrcFile}: ${npmrc}`);
# Private modules in Bitbucket Pipelines

Automates some of the steps described in the [Bitbucket Pipelines docs](https://confluence.atlassian.com/bitbucket/javascript-node-js-with-bitbucket-pipelines-873891287.html#Javascript(Node.js)withBitbucketPipelines-Downloadandpublishmodulestoaprivateregistry) to work with modules from a private NPM registry.

## Usage

First, [configure the following environment variable](https://confluence.atlassian.com/bitbucket/environment-variables-in-bitbucket-pipelines-794502608.html) in Bitbucket Pipelines:

* `NPM_TOKEN`: This is the authentication token to your registry. You can find it in your local ~/.npmrc, after you login to your registry

Your bitbucket-pipelines.yml script can then use `pipelines-private-npm` to configure the build container:

```yaml
image: node:carbon-slim

pipelines:
  default:
    - step:
        script:
          - npm i -g pipelines-private-npm
          - build-setup
          - npm install
          - ...
```

### Configuration parameters

The `build-setup` script accepts parameters to configure the registry, NPM organization and alias for the registry:

* `-r <registry>`: Configure the registry, e.g. `build-setup -r https://npm-private.example.com`.
* `-o <org>`: Configure the NPM organization for packages like `@example/my-package`. Example: `build-setup -r https://npm-private.example.com -o example`.
* `-a <alias>`: Configure an alias hostname for the registry specified with `-r`. This is useful for entries in `yarn.lock` that point to a specific registry host, but should be resolved against another host, e.g. a proxy.
* `-d`: Perform a dry run without modifying any files.

#### Example

Configure the `https://npm-private.example.com` registry with the `NPM_TOKEN` from the build environment for packages like `@example/my-package`. Ensure that requests for `https://npm-private.example.com` actually go to `https://npm-private-proxy.example.com`.

```bash
build-setup -r https://npm-private.example.com -o example -a npm-private-proxy.example.com
```